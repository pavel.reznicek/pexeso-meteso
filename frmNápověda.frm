VERSION 5.00
Begin VB.Form frmN�pov�da 
   Caption         =   "N�pov�da"
   ClientHeight    =   4485
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6390
   Icon            =   "frmN�pov�da.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4485
   ScaleWidth      =   6390
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtN�pov�da 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   0
      Width           =   4455
   End
End
Attribute VB_Name = "frmN�pov�da"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
   Dim Odst As String
   Odst = Chr(13) & Chr(10)
   txtN�pov�da = _
   "Pexeso Meteso" & Odst & _
   "������������" & Odst & _
   "   Pr�v� m�te pu�t�nu hru jm�nem PEXESO. " & _
   "Toto jm�no znamen�: �Pekeln� se soust�e�." & Odst & _
   "   Ano, budete se muset p�kn� soust�edit, " & _
   "abyste ji zvl�dli." & Odst & _
   "   Ve h�e je 64 karti�ek po p�rech. Va��m �kolem " & _
   "je naj�t co nejv�ce p�r� nebo v�ce p�r�, ne� " & _
   "protihr��, j�m� je v tomto p��pad� po��ta�." & Odst & _
   "   Karti�ky se ot��ej� klik�n�m na n�. V�dy " & _
   "m��ete oto�it najednou pouze dv� karti�ky." & Odst & _
   "   Pokud jste na�li p�r, pokra�ujete hled�n�m " & _
   "dal�� dvojice. Pokud jste se netrefili, je na " & _
   "tahu protihr��." & Odst & _
   "   Ve stavov� li�t� se v�m ukazuj� zpr�vy, " & _
   "kter� hodnot� v� v�kon a ��kaj� v�m, co m�te " & _
   "d�lat."
   txtN�pov�da.Locked = True
End Sub

Private Sub Form_Resize()
   txtN�pov�da.Move 0, 0, ScaleWidth, ScaleHeight
End Sub
