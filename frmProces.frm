VERSION 5.00
Begin VB.Form frmProces 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "L�duji obr�zky�"
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin Pexeso.UkazatelPom�ru ukProces 
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   661
      Max             =   100
      Value           =   0
      SmoothChange    =   -1  'True
      FillColor       =   12648384
      BackColor       =   -2147483633
   End
End
Attribute VB_Name = "frmProces"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    ' p�izp�soben� velikosti ukazateli
    Width = Width - ScaleWidth + ukProces.Width
    Height = Height - ScaleHeight + ukProces.Height
    ' p�ilepen� ukazatele
    ukProces.Move 0, 0
    ' vyst�ed�n� na hlavn� form
    'If frmPexeso.Visible Then
        Me.Left = frmPexeso.Left + frmPexeso.Width \ 2 - Me.Width \ 2
        Me.Top = frmPexeso.Top + frmPexeso.Top \ 2 - Me.Height \ 2
    'End If
    ' ur�en� maxima ukazatele
    DoEvents
    ukProces.Max = PocetX * PocetY / 2
End Sub

Public Property Let Stav(Nosi� As Integer)
    DoEvents
    ukProces = Nosi�
End Property
