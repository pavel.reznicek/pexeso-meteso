VERSION 5.00
Begin VB.UserControl UkazatelPom�ru 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   2700
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2775
   EditAtDesignTime=   -1  'True
   PropertyPages   =   "UkazatelPom�ru.ctx":0000
   ScaleHeight     =   180
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   185
   Begin VB.Shape obdBarv�k 
      BackStyle       =   1  'Opaque
      FillColor       =   &H80000002&
      FillStyle       =   0  'Solid
      Height          =   615
      Left            =   720
      Top             =   960
      Width           =   1215
   End
End
Attribute VB_Name = "UkazatelPom�ru"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "PropPageWizardRun" ,"Yes"
Dim flValue As Long
Dim flMax As Long
Dim flMove As Boolean
Dim flSmooth As Boolean
Const Interval = 100
Public Event Click()

Private Sub UserControl_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Enabled Then Exit Sub
    flMove = True
    If Max = 0 Or X <= 0 Then
        Value = 0
    ElseIf X >= UserControl.ScaleWidth Then
        Value = Max
    Else
        Value = Max / UserControl.ScaleWidth * X
    End If
    DoEvents
End Sub

Private Sub UserControl_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Enabled Then Exit Sub
    If flMove Then
        If Max = 0 Or X <= 0 Then
            Value = 0
        ElseIf X >= UserControl.ScaleWidth Then
            Value = Max
        Else
            Value = Max / UserControl.ScaleWidth * X
        End If
    End If
End Sub

Private Sub UserControl_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Not Enabled Then Exit Sub
    flMove = False
    If Max = 0 Or X <= 0 Then
        Value = 0
    ElseIf X >= UserControl.ScaleWidth Then
        Value = Max
    Else
        Value = Max / UserControl.ScaleWidth * X
    End If
    RaiseEvent Click
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    With PropBag
        Enabled = .ReadProperty("Enabled", True)
        Max = .ReadProperty("Max", 100)
        Value = .ReadProperty("Value", 0)
        SmoothChange = .ReadProperty("SmoothChange", True)
        FillColor = .ReadProperty("FillColor", obdBarv�k.FillColor)
        BackColor = .ReadProperty("BackColor", UserControl.BackColor)
    End With
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    With PropBag
        .WriteProperty "Enabled", Enabled, True
        .WriteProperty "Max", Max
        .WriteProperty "Value", Value
        .WriteProperty "SmoothChange", SmoothChange
        .WriteProperty "FillColor", FillColor
        .WriteProperty "BackColor", BackColor
    End With
End Sub

Private Sub UserControl_Resize()
    Value = flValue
End Sub

Public Property Let Enabled(Nosi� As Boolean)
    UserControl.Enabled = Nosi�
    PropertyChanged "Enabled"
End Property

Public Property Get Enabled() As Boolean
Attribute Enabled.VB_ProcData.VB_Invoke_Property = "Nastaven�"
    Enabled = UserControl.Enabled
End Property

Public Property Let Value(Kolik As Double)
'    If Kolik > 0 Then Stop
    flValue = Kolik
    Nakresli
    PropertyChanged "Value"
End Property

Public Property Get Value() As Double
Attribute Value.VB_ProcData.VB_Invoke_Property = "Nastaven�"
Attribute Value.VB_UserMemId = 0
Attribute Value.VB_MemberFlags = "200"
    Value = flValue
End Property

Public Property Let Max(Kolik As Double)
    flMax = Kolik
    Nakresli
    PropertyChanged "Max"
End Property

Public Property Get Max() As Double
Attribute Max.VB_ProcData.VB_Invoke_Property = "Nastaven�"
    Max = flMax
End Property

Public Property Let SmoothChange(Nosi� As Boolean)
    flSmooth = Nosi�
    PropertyChanged "SmoothChange"
End Property

Public Property Get SmoothChange() As Boolean
Attribute SmoothChange.VB_ProcData.VB_Invoke_Property = "Nastaven�"
    SmoothChange = flSmooth
End Property

Public Property Let FillColor(Barva As OLE_COLOR)
    obdBarv�k.FillColor = Barva
    PropertyChanged "FillColor"
End Property

Public Property Get FillColor() As OLE_COLOR
    FillColor = obdBarv�k.FillColor
End Property

Public Property Let BackColor(Barva As OLE_COLOR)
    UserControl.BackColor = Barva
    PropertyChanged "BackColor"
End Property

Public Property Get BackColor() As OLE_COLOR
    BackColor = UserControl.BackColor
End Property

Private Sub Nakresli()
    UserControl.Line (0, 0)-(���ka, UserControl.ScaleHeight), FillColor, BF
    UserControl.Line (���ka, 0)-(UserControl.ScaleWidth, UserControl.ScaleHeight), BackColor, BF
    Static Po��tadlo
    If SmoothChange Then
        DoEvents
    Else
        If Po��tadlo Mod Interval = 0 Then
            Po��tadlo = 0
            DoEvents
        End If
    End If
    Po��tadlo = Po��tadlo + 1
End Sub

Private Function ���ka()
    If Max = 0 Then
        ���ka = 0
    Else
        ���ka = (UserControl.ScaleWidth) * (Value / Max)
    End If
End Function
