# Pexeso Meteso

This is a Pairs/Concentration/Memory-type game 
(in Czechia known as PEXESO 
= an abbreviation of “**Pek**elně **se** **so**ustřeď”
= “Concentrate As Hell”).

Toto je PEXESO. Dále netřeba vysvětlovat. 😉

---

The code is written in VB6 with Czech comments and identifiers.

Kód je psán ve VB6 s českými komentáři a identifikátory.

---

The main feature is the ability to select the images 
from wherever on your computer, even across folders.

Hlavní výhodou je možnost vybrat obrázky
odkudkoliv z vašeho počítače, i napříč adresáři.

---

Thanks go to Jan Holý which is the original author.

Díky patří Janu Holému, který je původním autorem.

---

Pavel Řezníček improved the game a bit and created this Git repository.

Pavel Řezníček hru trochu vylepšil a vytvořil tento gitový repositář.

---

This game is licensed under the terms of the GNU/GPL v. 3.

Tato hra je licencována pod podmínkami GNU/GPL, v. 3.

---

If the Pexeso Meteso game cought your heart you can [donate](https://www.paypal.com/donate/?business=CEMCJ8YTJY7HA&no_recurring=0&item_name=V%C3%BDvoj+u%C5%BEite%C4%8Dn%C3%A9ho+a+z%C3%A1bavn%C3%A9ho+svobodn%C3%A9ho+softwaru+pro+pomoc+i+pro+radost&currency_code=CZK) for its development.

Vzalo-li vás Pexeso Meteso za srdce, můžete [přispět](https://www.paypal.com/donate/?business=CEMCJ8YTJY7HA&no_recurring=0&item_name=V%C3%BDvoj+u%C5%BEite%C4%8Dn%C3%A9ho+a+z%C3%A1bavn%C3%A9ho+svobodn%C3%A9ho+softwaru+pro+pomoc+i+pro+radost&currency_code=CZK) na jeho vývoj.

![Donate/Přispět](qrcode-normal.png)