VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmPexeso 
   Caption         =   "Pexeso Meteso"
   ClientHeight    =   4860
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   6435
   Icon            =   "Pexeso.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   324
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   429
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.StatusBar sbrStav 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   4605
      Width           =   6435
      _ExtentX        =   11351
      _ExtentY        =   450
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   3
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            AutoSize        =   1
            Object.Width           =   10240
            MinWidth        =   265
            TextSave        =   ""
            Key             =   ""
            Object.Tag             =   ""
         EndProperty
         BeginProperty Panel2 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            TextSave        =   ""
            Key             =   ""
            Object.Tag             =   ""
         EndProperty
         BeginProperty Panel3 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            AutoSize        =   2
            Object.Width           =   265
            MinWidth        =   265
            TextSave        =   ""
            Key             =   ""
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog ComDialAdr 
      Left            =   3720
      Top             =   1320
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      MaxFileSize     =   10000
   End
   Begin VB.Image Obr 
      BorderStyle     =   1  'Fixed Single
      Height          =   1215
      Index           =   0
      Left            =   0
      Picture         =   "Pexeso.frx":0442
      Stretch         =   -1  'True
      Top             =   0
      Width           =   1335
   End
   Begin VB.Menu nabHra 
      Caption         =   "&Hra"
      Begin VB.Menu nabVybrat 
         Caption         =   "&Vybrat obr�zky"
         Shortcut        =   ^O
      End
      Begin VB.Menu nabRozdat 
         Caption         =   "&Rozdat"
         Shortcut        =   ^R
      End
      Begin VB.Menu nabSep0 
         Caption         =   "-"
      End
      Begin VB.Menu nabKonec 
         Caption         =   "&Konec"
         Shortcut        =   ^K
      End
   End
   Begin VB.Menu nabProt 
      Caption         =   "&Protivn�k"
      Begin VB.Menu nabProtivn�k 
         Caption         =   "��&dn�"
         Index           =   0
      End
      Begin VB.Menu nabProtivn�k 
         Caption         =   "&Blboun nejapn�"
         Checked         =   -1  'True
         Index           =   1
      End
      Begin VB.Menu nabProtivn�k 
         Caption         =   "B&lboun v�ev�d"
         Index           =   2
      End
      Begin VB.Menu nabProtivn�k 
         Caption         =   "&D�d v�ev�d"
         Index           =   3
      End
   End
   Begin VB.Menu nabN�p 
      Caption         =   "&N�pov�da"
      Begin VB.Menu nabN�pov�da 
         Caption         =   "&N�pov�da"
         Shortcut        =   {F1}
      End
   End
End
Attribute VB_Name = "frmPexeso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Option Base 0
   Dim PocetObr As Integer, ZbyvaObr As Integer ' pocty
   Dim PoleObr() As String    ' pole souboru obrazku
   Dim KolObr As New Collection ' kolekce nal�dovan�ch obr�zk�
   Dim Rub As IPictureDisp    ' obr�zek na rubu
   Dim N�hKolObr As New Collection ' prom�chan� obr�zky
   Dim IndObr As Integer   ' aktualni index obrazku
   Dim ZbylyStrVolby As String  ' zbyly string volby
   Dim T�hl�lov�k As Boolean  ' p��znak, kdo t�hl
   Dim pamStav�lo As Integer  ' stav �lov�ka
   Dim pamStavPo� As Integer  ' stav po��ta�e

Private Sub Form_Click()
    Obr_Click -1
End Sub

Private Sub Form_Load()
   Dim i
   ' nata�en� rubu
   Set Rub = Obr(0).Picture
   ' vytvoreni obrazku (image)
   Dim ���, V��, ZbX, ZbY
   sbrStav.Height = 18
   ��� = Me.ScaleWidth \ PocetX
   V�� = (Me.ScaleHeight - sbrStav.Height) \ PocetY
   ZbX = Me.ScaleWidth Mod PocetX
   ZbY = (Me.ScaleHeight - sbrStav.Height) Mod PocetY
   Obr(0).Move ZbX \ 2, ZbY \ 2, ���, V��
   For i = 1 To PocetX * PocetY - 1
      Load Obr(i)
      Obr(i).Visible = True
   Next i
   VyberObr�zky
   Protivn�k = GetSetting("Pexeso", "Nastaven�", "Protivn�k", 1)
End Sub

   Private Function OdectiPrvniSlovo() As String
      Dim PosMez As Integer   ' pozice mezery
      Dim Delka As Integer    ' delka stringu
      ' najiti mezery
      PosMez = InStr(ZbylyStrVolby, Chr(0))
      ' poznamka delky slova
      Delka = Len(ZbylyStrVolby)
      If PosMez > 0 Then   ' mezera nalezena
         ' naplneni hodnotou
         OdectiPrvniSlovo = Left(ZbylyStrVolby, PosMez - 1)
         ' zmenseni
         ZbylyStrVolby = Right(ZbylyStrVolby, Delka - PosMez)
      Else     ' mezera nenalezena
         ' naplneni zbytnou hodnotou
         OdectiPrvniSlovo = ZbylyStrVolby
         ' definitivni odpis
         ZbylyStrVolby = ""
      End If
   End Function
   
   Private Sub DoplnObrazek(ByVal Ces As String)
      Dim Nepridavat As Boolean  ' nevezmeme ho
      Dim i As Integer
      ' logicke kontroly
         ' neni cesta
         If Ces = "" Then Nepridavat = True
         ' obrazek se opakuje
         For i = 0 To IndObr - 1
            If PoleObr(i) = Ces Then Nepridavat = True
         Next i
      ' doplneni obrazku
      If Not Nepridavat Then
         ' doplneni obrazku na nasledujici misto
         PoleObr(IndObr) = Ces
         ' posun indexu
         IndObr = IndObr + 1
         ' zbyvajici pocet obrazku
         ZbyvaObr = ZbyvaObr - 1
      End If
   End Sub
   
    Private Sub Nal�dujObr�zky()
        Dim i, Ces As String
        frmProces.Show , Me
        For i = 0 To PocetObr - 1
            ' p�id�n� obr�zku do kolekce
            Ces = PoleObr(i)
            KolObr.Add LoadPicture(Ces)
            ' grafick� zn�zorn�n� procesu
            frmProces.Stav = i + 1
        Next
        Unload frmProces
    End Sub
    
    Private Sub Naho�Obr�zky()
        Dim i
        Dim St�elec As New GenerN ' nov� gener�tor n�hody
        Dim Obsazeno As Boolean
        Dim St�ela As Integer ' navr�enej index obr�zku
        Dim ObrObj As Object
        Dim KolikNa�el As Integer ' kolik jich na�el: jeden nebo dva?
        Set N�hKolObr = Nothing
        Set N�hKolObr = New Collection
        St�elec.Smallest = 1
        St�elec.Largest = PocetObr
        For i = 1 To PocetObr * 2
            ' smy�ka, kter� b��, dokud gener�tor
            ' nenavrhne neobsazenej obr�zek
            Do
                KolikNa�el = 0
                St�ela = St�elec.Value
                For Each ObrObj In N�hKolObr
                    ' zda obr�zek s indexem u� je p�id�n
                    ' - pokud n�co najde, zaznamenat
                    If ObrObj Is KolObr(St�ela) Then
                        KolikNa�el = KolikNa�el + 1
                    End If
                    ' pokud na�el 2, je to blbej n�vrh
                    ' - ��slo je obsazeno
                    ' - hled�me d�l
                    If KolikNa�el >= 2 Then
                        Obsazeno = True
                        Exit For
                    ' pokud nena�el 2, je to dobrej n�vrh
                    ' - ��slo nen� obsazeno
                    ' - vezmeme to, konec hled�n�
                    Else
                        Obsazeno = False
                    End If
                Next
            Loop While Obsazeno
            ' Na�li jsme. Te� to kone�n� m��eme p�idat
            ' do n�hodn� kolekce.
            N�hKolObr.Add KolObr(St�ela)
        Next
        For Each ObrObj In Obr
            ObrObj.Visible = True
            ObrObj.Picture = Rub
        Next
        Zpr�va = "Vyber prvn� karti�ku. Klikni na ni."
        Stav�lo = 0
        StavPo� = 0
    End Sub

Private Sub Form_Resize()
   If Me.WindowState = vbMinimized Then Exit Sub
   Dim i, j As Integer
   Dim Sir, Vys As Integer
   Dim posX, posY As Integer
   Dim ZbX, ZbY As Integer
   ' zakladni vypocet rozmeru obrazku
   Sir = Me.ScaleWidth \ PocetX
   Vys = (Me.ScaleHeight - sbrStav.Height) \ PocetY
   If Vys < 0 Then Exit Sub
   ZbX = Me.ScaleWidth Mod PocetX
   ZbY = (Me.ScaleHeight - sbrStav.Height) Mod PocetX
   ' umisteni obrazku
   For i = 0 To PocetX - 1
      For j = 0 To PocetY - 1
         posX = Sir * i + ZbX \ 2
         posY = Vys * j + ZbY \ 2
         Obr(i + j * PocetX).Move posX, posY, Sir, Vys
      Next j
   Next i
End Sub

Private Sub nabKonec_Click()
    End
End Sub

Private Sub nabN�pov�da_Click()
   frmN�pov�da.Show , Me
End Sub

Private Sub nabProtivn�k_Click(Index As Integer)
   Protivn�k = Index
End Sub

Private Property Let Protivn�k(Index As Integer)
   Dim Nab As Menu
   For Each Nab In nabProtivn�k
      If Nab.Index = Index Then
         Nab.Checked = True
         SaveSetting "Pexeso", "Nastaven�", "Protivn�k", Index
      Else
         Nab.Checked = False
      End If
   Next
End Property

Private Property Get Protivn�k() As Integer
   Dim Nab As Menu
   For Each Nab In nabProtivn�k
      If Nab.Checked Then
          Protivn�k = Nab.Index
      End If
   Next
End Property

Private Sub nabRozdat_Click()
    Naho�Obr�zky
End Sub

Private Sub nabVybrat_Click()
    VyberObr�zky
End Sub

Sub VyberObr�zky()
   Dim i As Integer
   Dim Cesta, Soubor As String  ' spolecna cesta pro multivyber
   ' priprava k vytvareni pole obrazku
   ComDialAdr.Flags = cdlOFNAllowMultiselect + cdlOFNExplorer + cdlOFNHideReadOnly
   ComDialAdr.CancelError = True
   ComDialAdr.Filter = "v�eci�ky obr�zky|*.bmp;*.jpg;*.gif;*.ico;*.cur"
   PocetObr = PocetX * PocetY / 2
   ReDim PoleObr(PocetObr - 1)
   ZbyvaObr = PocetObr
   IndObr = 0
   Set KolObr = Nothing
   Set KolObr = New Collection
   ' vytvoreni pole obrazku
   Do While ZbyvaObr > 0
      ComDialAdr.DialogTitle = "Pexeso. Vyber " & ZbyvaObr & " obr�zk�"
      ComDialAdr.FileName = ""
      On Error GoTo vypadni
      ComDialAdr.ShowOpen
      On Error GoTo 0
      ' doplneni vybranych obrazku do pole
         ' zapis vybraneho stringu
         ZbylyStrVolby = ComDialAdr.FileName
         ' zjisteni cesty
         Cesta = OdectiPrvniSlovo
         ' vyber jednoho nebo vice souboru?
         If ZbylyStrVolby = "" Then ' jednovolba
            DoplnObrazek (Cesta)
         Else  ' multivolba
            ' samotne doplnovani souboru
            Do While ZbyvaObr > 0 And ZbylyStrVolby <> ""
               ' soubor
               Soubor = OdectiPrvniSlovo
               ' doplneni obrazku
               DoplnObrazek (Cesta & "\" & Soubor)
            Loop
         End If
   Loop
   Nal�dujObr�zky
   Naho�Obr�zky
   nabRozdat.Enabled = True
   Exit Sub
vypadni:
   nabRozdat.Enabled = False
End Sub

Private Sub Obr_Click(Index As Integer)
   Static Oto�it As Boolean
   Dim KolikNa�el As Integer
   
   ' on line natazeni nebo stazeni obrazku
   If M��eT�hnout Then
      If JeObr(Index) Then
         'Obr(Index).Picture = Rub
      ' pokud se nekliklo na form
      ElseIf Index > -1 Then
         ' pokud je k disposici obr�zek, obra�
         If Not N�hKolObr.Count = 0 Then
            Obr(Index).Picture = N�hKolObr(Index + 1)
            T�hl�lov�k = True
         End If
      End If
      If CoJeOto�eno(2) > -1 Then
         If �sp�ch Then
            Zpr�va = "�sp�ch! Klikni pro schov�n� karti�ek."
         Else
            Zpr�va = "Ne, ne. Klikni pro " & IIf(Protivn�k > 0, "m�j", "dal��") & " tah."
         End If
      ElseIf CoJeOto�eno(1) > -1 Then
         Zpr�va = "Tak kde je druh� do p�ru? Klikni na ni."
      End If
   ' z�kaz pro �lov�ka t�hnout: dv� karty vybr�ny -> hodnocen�
   Else
      ' pokud je to spr�vn�, skryj to
      If �sp�ch Then
         SkryjP�r CoJeOto�eno(1), CoJeOto�eno(2)
         ' kontrola, jestli u� je v�echno skryto -> m��eme kon�it
         If Hotovo Then
            Zpr�va = "Hotovo!"
            Dim Vyhr�l As String, Skore As String
            Vyhr�l = IIf(Stav�lo > StavPo�, "Vyhr�l jsi ", "Prohr�l jsi ")
            Skore = Stav�lo & ":" & StavPo� & "."
            If Protivn�k > 0 Then ' s protivn�kem
               MsgBox "Hotovo! " & Vyhr�l & Skore, vbInformation
            Else ' bez protivn�ka
               MsgBox "Hotovo! Na�el jsi v�echny p�ry."
            End If
            Exit Sub
         End If
         ' pokud t�hl po��ta�, t�hni, po��ta�i, znovu
         If Not T�hl�lov�k Then
            TahPo��ta�e
            ' zkontroluj si v�sledek
            If �sp�ch Then
               Zpr�va = "Hur�, m�m to! Klikni pro m�j dal�� tah."
            Else
               Zpr�va = "Tak to vid�. Klikni pro sv�j tah."
            End If
         ' pokud t�hl �lov�k, �ekni, aby t�hl znovu
         Else
            Zpr�va = "Jsi je�t� na tahu. Vyber prvn� karti�ku."
         End If
      ' pokud je to �patn�, oto� to na rub
      Else
         Polo�P�r CoJeOto�eno(1), CoJeOto�eno(2)
         If T�hl�lov�k And Protivn�k > 0 Then
            TahPo��ta�e
         Else
            Zpr�va = "Vyber prvn� karti�ku. Klikni na ni."
         End If
      End If
   End If
End Sub

Sub SkryjP�r(Obr1 As Integer, Obr2 As Integer)
   Obr(Obr1).Visible = False
   Obr(Obr1).Picture = Rub
   Obr(Obr2).Visible = False
   Obr(Obr2).Picture = Rub
   If T�hl�lov�k Then
      Stav�lo = Stav�lo + 1
   Else
      StavPo� = StavPo� + 1
   End If
End Sub

Sub Polo�P�r(Obr1 As Integer, Obr2 As Integer)
   Obr(Obr1).Picture = Rub
   Obr(Obr2).Picture = Rub
End Sub

Sub TahPo��ta�e()
   If Protivn�k = 0 Then Exit Sub
   Dim GenKarty      As New GenerN ' gener�tor ��sla karty
   Dim GenZpTahu     As New GenerN ' gener�tor zp�sobu tahu
   Dim PamZpTahu     As Integer ' pamatov�k na hodnotu GenZpTahu
   Dim Karta(1 To 2) As Integer ' pamatov�k zvolen� karty
   Dim TahV�ev�da    As Boolean ' jestli bude v�ev�douc�
   Dim O As Image
   ' nastaven� p��znaku hr��e
   T�hl�lov�k = False
   ' nastaven� gener�tor�
   GenKarty.Smallest = 0
   GenKarty.Largest = PocetObr * 2 - 1
      ' �tvrtinov� n�hoda
   GenZpTahu.Smallest = 1
   GenZpTahu.Largest = 4
   ' v�b�r zp�sobu tahu
   Select Case Protivn�k
   ' blboun nejapn�: �pln� n�hodn�
   Case 1
      TahV�ev�da = False
   ' blboun v�ev�d: n�kdy n�hoda, n�kdy tutovka
   Case 2
      PamZpTahu = GenZpTahu
      ' ve �tvrtin� p��pad� v�ev�d
      TahV�ev�da = IIf(PamZpTahu = 1, True, False)
   ' d�d v�ev�d: v�dycky tutovka
   Case 3
      TahV�ev�da = True
   End Select
   ' blboun
   If Not TahV�ev�da Then
      ' najde n�hodnou kartu, kter� je ve h�e
      ' (je vid�t)
      Do
         Karta(1) = GenKarty
      Loop Until Obr(Karta(1)).Visible
      ' uk�e 1. obr�zek
      Obr(Karta(1)).Picture = N�hKolObr(Karta(1) + 1)
      ' najde n�hodnou kartu, kter� je ve h�e
      ' a neni to ta prvn�
      Do
         Karta(2) = GenKarty
      Loop Until Obr(Karta(2)).Visible And (Karta(1) <> Karta(2))
      ' uk�e 2. obr�zek
      Obr(Karta(2)).Picture = N�hKolObr(Karta(2) + 1)
   ' v�ev�d
   Else
      ' najde n�hodnou kartu, kter� je ve h�e
      ' (je vid�t)
      Do
         Karta(1) = GenKarty
      Loop Until Obr(Karta(1)).Visible
      ' uk�e 1. or�zek
      Obr(Karta(1)).Picture = N�hKolObr(Karta(1) + 1)
      ' najde p�esn� druhou do p�ru
      For Each O In Obr
         If (N�hKolObr(O.Index + 1) Is N�hKolObr(Karta(1) + 1)) _
         And Not (O.Index = Obr(Karta(1)).Index) Then
            O.Picture = N�hKolObr(O.Index + 1)
            Exit For
         End If
      Next
   End If
   ' zkontroluj si v�sledek
   If �sp�ch Then
      Zpr�va = "Hur�, m�m to! Klikni pro m�j dal�� tah."
   Else
      Zpr�va = "Tak to vid�. Klikni pro sv�j tah."
   End If
End Sub

Function JeObr(Index As Integer) As Boolean
   ' jestli je tam obr�zek, zjist�me provn�n�m s pr�zdn�m obr�zkem
   If Index = -1 Then Exit Function
   JeObr = Not (Obr(Index).Picture Is Rub)
End Function

Function M��eT�hnout() As Boolean
   Dim O As Image
   Dim KolikNa�el
   For Each O In Obr
      If JeObr(O.Index) Then KolikNa�el = KolikNa�el + 1
   Next
   M��eT�hnout = (KolikNa�el < 2)
End Function

Function �sp�ch() As Boolean
   If CoJeOto�eno(1) = -1 Or CoJeOto�eno(2) = -1 Then Exit Function
   �sp�ch = Obr(CoJeOto�eno(1)).Picture Is Obr(CoJeOto�eno(2)).Picture
End Function

Function CoJeOto�eno(Po�ad� As Integer) As Integer
   Dim O As Image
   Dim KolikNa�el
   For Each O In Obr
      If JeObr(O.Index) Then KolikNa�el = KolikNa�el + 1
      If KolikNa�el = Po�ad� Then
         CoJeOto�eno = O.Index
         Exit For
      End If
   Next
   If KolikNa�el < Po�ad� Then CoJeOto�eno = -1
   If Po�ad� < 1 Then MsgBox "Neptej se, co jsem na�el, kdy� ��k�, �e jsem nena�el nic.", vbExclamation: Debug.Assert False
   If Po�ad� > 2 Then MsgBox "Neptej se, co jsem na�el, kdy� se tady toho ned� tolik naj�t", vbExclamation: Debug.Assert False
End Function

Function Hotovo() As Boolean
   Dim O As Image
   Hotovo = True
   For Each O In Obr
      If O.Visible Then
         Hotovo = False
         Exit For
      End If
   Next
End Function

Property Let Zpr�va(Nosi� As String)
   sbrStav.Panels(1) = Nosi�
End Property

Property Let Stav�lo(Nosi� As Integer)
   sbrStav.Panels(2) = "M� p�r�: " & Nosi�
   pamStav�lo = Nosi�
End Property

Property Get Stav�lo() As Integer
   Stav�lo = pamStav�lo
End Property

Property Let StavPo�(Nosi� As Integer)
   sbrStav.Panels(3) = "Po��ta� m� p�r�: " & Nosi�
   pamStavPo� = Nosi�
End Property

Property Get StavPo�() As Integer
   StavPo� = pamStavPo�
End Property
