VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "GenerN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' Deklarace vlastnost�, kter� budou ur�ovat rozsah
Public Smallest As Long
Public Largest As Long

' V�sledn� pseudon�hodn� ��slo
Public Property Get Value()
Attribute Value.VB_UserMemId = 0
    Value = Int(Rnd * (Largest - Smallest + 1)) + Smallest
End Property

' Inicializace n�hodn� posloupnosti
Private Sub Class_Initialize()
    Randomize
End Sub
